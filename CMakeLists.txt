# Set minimal cmake version
cmake_minimum_required( VERSION
	3.1.3 )

# Dossier de sortie
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY
	${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY
	${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY
	${CMAKE_BINARY_DIR}/../Output )

# Fichiers sources NLib
file( GLOB_RECURSE
	NLIB_SOURCE_FILES
	../../NLib/NLib/src/*.c )
file( GLOB_RECURSE
	NLIB_HEADER_FILES
	../../NLib/NLib/include/*.h )

# Fichiers sources serveur http
file( GLOB_RECURSE
	NSSH_SOURCE_FILES
	src/*.c )
file( GLOB_RECURSE
	NSSH_HEADER_FILES
	include/*.h )

# Projet NHTTP
project( NTESTSSHSFTP_PROJECT )
	# Sources test SSH
	add_executable( NTestSSH
		${NLIB_SOURCE_FILES}
		${NLIB_HEADER_FILES}
		${NSSH_SOURCE_FILES}
		${NSSH_HEADER_FILES} )

	# Activer le module reseau
	target_compile_definitions( NTestSSH
		PUBLIC
		NLIB_MODULE_RESEAU )

	# Activer le module repertoires
	target_compile_definitions( NTestSSH
		PUBLIC
		NLIB_MODULE_REPERTOIRE )

	# Activer le module ssh
	target_compile_definitions( NTestSSH
		PUBLIC
		NLIB_MODULE_SSH )

	# Link librairies
	target_link_libraries( NTestSSH
		pthread )
	target_link_libraries( NTestSSH
		ssh2 )
	target_link_libraries( NTestSSH
		crypto )