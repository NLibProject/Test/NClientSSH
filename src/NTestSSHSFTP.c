#include "../include/NTestSSHSFTP.h"

// ----------------------
// namespace NTestSSHSFTP
// ----------------------

#define NTESTSSHSFTP_TIMEOUT	500

/**
 * Point d'entree
 *
 * @param argc
 * 		Le nombre d'arguments
 * @param argv
 * 		Le vecteur d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passess
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Client SSH
	NClientSSH *client;

	// Authentification
	NMethodeAuthentificationSSH *authentification;

	// Sortie
	char *sortie;

	// Initialiser
	NLib_Initialiser( NULL );

	// Creer authentification
	authentification = NLib_Module_SSH_NMethodeAuthentificationSSH_Construire2( "lucas",
		"",
		NFALSE,
		"key.pem",
		NTRUE );

	// Creer client
	if( ( client = NLib_Module_SSH_NClientSSH_Construire( "127.0.0.1",
		22,
		authentification,
		NMODE_SSH_EXEC ) ) != NULL )
	{
		// Lire retour
		while( !NLib_Module_SSH_NClientSSH_EstExecutionTermine( client ) )
		{
			/*if( ( sortie = NLib_Module_SSH_NClientSSH_LireFlux( client,
				NTESTSSHSFTP_TIMEOUT ) ) != NULL )
				puts( sortie );
			NFREE( sortie );*/
			NLib_Module_SSH_NClientSSH_FlushEntree( client );
		}

		// Executer commande test
		NLib_Module_SSH_NClientSSH_Executer( client,
			"sudo apt update" ); // "echo \"Wesh la forme?\" && sleep 5 && echo \"Ca va bien?\" && sleep 5\n" );

		// Lire retour
		while( !NLib_Module_SSH_NClientSSH_EstExecutionTermine( client ) )
		{
			if( ( sortie = NLib_Module_SSH_NClientSSH_LireFlux( client,
				NTESTSSHSFTP_TIMEOUT ) ) != NULL )
				printf( sortie );
			fflush( stdout );
			NFREE( sortie );
		}

		// Executer commande test
		NLib_Module_SSH_NClientSSH_Executer( client,
			"echo \"XPTDR?\" >> /tmp/test\n" );

		// Lire retour
		while( !NLib_Module_SSH_NClientSSH_EstExecutionTermine( client ) )
		{
			/*if( ( sortie = NLib_Module_SSH_NClientSSH_LireFlux( client,
				NTESTSSHSFTP_TIMEOUT ) ) != NULL )
				puts( sortie );
			NFREE( sortie );*/
			NLib_Module_SSH_NClientSSH_FlushEntree( client );
		}

		// Executer commande test
		NLib_Module_SSH_NClientSSH_Executer( client,
			"echo Bonjour\n" );

		// Lire retour
		while( !NLib_Module_SSH_NClientSSH_EstExecutionTermine( client ) )
		{
			if( ( sortie = NLib_Module_SSH_NClientSSH_LireFlux( client,
				NTESTSSHSFTP_TIMEOUT ) ) != NULL )
				puts( sortie );
			NFREE( sortie );
		}

		// Detruire client
		NLib_Module_SSH_NClientSSH_Detruire( &client );
	}

	// Detruire
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

